using System.Collections.Generic;

namespace leggylegit;

using System;
using Godot;

/// One of the four edges of a surface, indexed counter-clockwise, starting with the x-axis.
public enum Edge { X0 = 0, X1 = 1, Y1 = 2, Y0 = 3 }

public static class EdgeMethods
{
    public static bool IsX (this Edge e) => e == Edge.X0 || e == Edge.X1;
    public static bool IsY (this Edge e) => e == Edge.Y0 || e == Edge.Y1;
}

/// A position in space. This is just one grid square on a specified (SID) surface.
struct SpaceIndex
{
    public int SID;
    public int X;
    public int Y;

    public SpaceIndex(int sid, int x, int y)
    {
        SID = sid;
        X = x;
        Y = y;
    }
}

/// A sub-section of an edge.
/// Use `Surface.SubEdge` to obtain one.
struct SubEdge
{
    public Surface Surface;
    public Edge Edge;
    public int Start;
    public int Stop;

    public SubEdge(Surface s, Edge e, int start, int stop)
    {
        Surface = s;
        Edge = e;
        Start = start;
        Stop = stop;
    }

    public int Length => Math.Abs(Stop - Start);
    public int Orientation => Math.Sign(Stop - Start);

    /// Map integer index to a coordinate in surface-space.
    public SpaceIndex ToSurface(int i)
    {
        var edgeOffset = Surface.EdgeOffset(Edge);
        var axisIndex = Start + Orientation * i;
        if (Edge.IsX()) return new SpaceIndex(Surface.SID, axisIndex, edgeOffset.y);
        else            return new SpaceIndex(Surface.SID, edgeOffset.x, axisIndex);
    }
}

/// A surface is a rectangular grid. It may be connected to other surfaces via `Connection`s.
/// Use the `Space` API to CRUD, this object is read-only.
struct Surface
{
    public int SID;
    public int Width;
    public int Height;

    public Surface (int sid, int width, int height)
    {
        SID = sid;
        Width = width;
        Height = height;
    }

    /// Get offsets for edges.
    public Vector2i EdgeOffset (Edge edge)
    {
        switch (edge)
        {
            case Edge.X0: return new Vector2i(0, 0);
            case Edge.Y0: return new Vector2i(0, 0);
            case Edge.X1: return new Vector2i(0, Height);
            case Edge.Y1: return new Vector2i(Width, 0);
            default: throw new InvalidOperationException();
        }
    }
    
    ///  Construct a SubEdge for this surface.
    public SubEdge SubEdge (Edge edge, int start, int stop)
    {
        var length = Math.Abs(stop - start);
        // Validation: length must not be zero.
        if (length == 0) throw new ArgumentException("SubEdge must not have zero length.");
        // Validation: must be within the bounds of the surface dimensions.
        if (
            (edge == Edge.X0 || edge == Edge.X1) && (start < 0 || start >= Width || stop < 0 || stop >= Width) ||
            (edge == Edge.Y0 || edge == Edge.Y1) && (start < 0 || start >= Height || stop < 0 || stop >= Height)
        )
            throw new ArgumentException("SubEdge cannot be greater than the dimensions of the surface.");
        return new SubEdge(this, edge, start, stop);
    }
}

/// A connection is a link between two surfaces.
/// Use the `Space` API to CRUD, this object is read-only.
struct Connection
{
    public Surface S1;
    public SubEdge E1;
    public Surface S2;
    public SubEdge E2;

    public Connection (Surface s1, SubEdge e1, Surface s2, SubEdge e2)
    {
        // Validation: lengths must be the same.
        if (e1.Length != e2.Length) throw new ArgumentException("SubEdge lengths must be the same.");
        S1 = s1;
        E1 = e1;
        S2 = s2;
        E2 = e2;
    }
    
    public int Length => E1.Length;
    // TODO: Connection `orientation`? What does that mean precisely? Suspicion: -1 iff SubEdge orientations are different.
    // TODO: This might not be totally correct...
    public int Orientation => E1.Orientation == E2.Orientation ? 1 : -1;
}

/// The four indices surrounding some index. These are nullable as not all points in a neighbourhood need exist, for
/// instance if we're on the edge of a surface with no connection.
/// Use the `Space` API to obtain one.
struct SpaceNeighbourhood
{
    public SpaceIndex? XP = null;
    public SpaceIndex? XN = null;
    public SpaceIndex? YP = null;
    public SpaceIndex? YN = null;
}

/// Top-level container for the topology of the world.
class Space
{
    private Dictionary<int, Surface> _surfaces;
    private List<Connection> _connections;
}